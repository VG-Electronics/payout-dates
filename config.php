<?php

require_once('vendor/autoload.php');

use Allop\TimeValue\Day;

return [
    'base' => [
        'previousDayOfWeek' => Day::Friday
    ],
    
    'bonus' => [
        'dayOfMonth' => 15,
        'nextDayOfWeek' => Day::Wednesday
    ],
];