<?php

namespace Allop\TimeValue;

class Month
{
    public const January = 1;
    public const February = 2;
    public const March = 3;
    public const April = 4;
    public const May = 5;
    public const June = 6;
    public const July = 7;
    public const August = 8;
    public const September = 9;
    public const October = 10;
    public const November = 11;
    public const December = 12;

    private $names = [
        self::January => 'Styczeń',
        self::February => 'Luty',
        self::March => 'Marzec',
        self::April => 'Kwiecień',
        self::May => 'Maj',
        self::June => 'Czerwiec',
        self::July => 'Lipiec',
        self::August => 'Sierpień',
        self::September => 'Wrzesień',
        self::October => 'Październik',
        self::November => 'Listopad',
        self::December => 'Grudzień',
    ];

    private $value;

    public function __construct(int $monthNumber)
    {
        $this->validate($monthNumber);

        $this->value = $monthNumber;
    }

    public function getMonthName(): string
    {
        return $this->names[$this->value];
    }

    public function value(): int
    {
        return $this->value;
    }

    private function validate(int $monthNumber): void
    {
        if ($monthNumber < 1 || $monthNumber > 12) {
            throw new \Exception("Month must be a number between 1 and 12.");
        }
    }
}
