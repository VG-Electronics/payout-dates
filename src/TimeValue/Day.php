<?php

namespace Allop\TimeValue;

class Day
{
    public const Monday = 1;
    public const Tuesday = 2;
    public const Wednesday = 3;
    public const Thursday = 4;
    public const Friday = 5;
    public const Saturday = 6;
    public const Sunday = 7;

    public static $names = [
        self::Monday => 'Poniedziałek',
        self::Tuesday => 'Wtorek',
        self::Wednesday => 'Środa',
        self::Thursday => 'Czwartek',
        self::Friday => 'Piątek',
        self::Saturday => 'Sobota',
        self::Sunday => 'Niedziela',
    ];

    private $value;

    public function __construct(int $dayNumber)
    {
        $this->validate($dayNumber);

        $this->value = $dayNumber;
    }

    public static function getDayName(int $dayNumber): string
    {
        return self::$names[$dayNumber];
    }

    public function value(): int
    {
        return $this->value;
    }

    private function validate(int $dayNumber): void
    {
        if ($dayNumber < 1 || $dayNumber > 31) {
            throw new \Exception("Day must be a number between 1 and 31.");
        }
    }
}
