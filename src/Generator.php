<?php

namespace Allop;

use Allop\PaymentDate\BasePaymentDate;
use Allop\PaymentDate\BonusPaymentDate;
Use Allop\TimeValue\Month;

class Generator
{
    public static $labels = [
        'Miesiąc',
        'Dzień wypłaty podstawowej',
        'Dzień wypłaty premii'
    ];

    public static function preparePaymentDates(bool $fullYear): array
    {
        $months = $fullYear ? range(1, 12) : self::getMonthsLeftFromNow();

        $data = [self::$labels];

        foreach ($months as $month) {
            $month = new Month($month);

            $basePaymentDate = new BasePaymentDate($month);
            $bonusPaymentDate = new BonusPaymentDate($month);

            if (!$fullYear && $basePaymentDate->date->isPast() && $bonusPaymentDate->date->isPast()) {
                continue;
            }

            $data[] = [
                $month->getMonthName(),
                $basePaymentDate->getDateLabel(),
                $bonusPaymentDate->getDateLabel(),
            ];
        }

        return $data;
    }

    private static function getMonthsLeftFromNow(): array
    {
        $currentMonth = date('m');

        return range($currentMonth, 12);
    }
}
