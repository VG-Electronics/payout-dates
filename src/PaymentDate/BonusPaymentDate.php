<?php
namespace Allop\PaymentDate;

Use Allop\TimeValue\Month;
Use Allop\TimeValue\Day;

class BonusPaymentDate extends PaymentDate 
{
    private $dayOfMonth = 15;
    private $nextDayOfWeek = Day::Wednesday;

    public function __construct(Month $month)
    {
        $config = $this->getConfig()['bonus'];
        $this->dayOfMonth = $config['dayOfMonth'];
        $this->nextDayOfWeek = $config['nextDayOfWeek'];

        $defaultDate = $this->getCarbonDate($month, new Day($this->dayOfMonth));

        $this->date = $defaultDate->isWeekend() ? $defaultDate->next($this->nextDayOfWeek) : $defaultDate;
    }
}