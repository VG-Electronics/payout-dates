<?php

namespace Allop\PaymentDate;

use Allop\TimeValue\Month;
use Allop\TimeValue\Day;

use Carbon\Carbon;

class PaymentDate
{
    public function getDateLabel(): string
    {
        return $this->getDayNumber() . ' ' . $this->getDayName();
    }

    public function getDayNumber(): int
    {
        return $this->date->day;
    }

    public function getDayName(): string
    {
        return Day::getDayName($this->date->dayOfWeek);
    }

    protected function getCarbonDate(Month $month, Day $day = null): Carbon
    {
        return Carbon::create(date('Y'), $month->value(), $day ? $day->value() : null);
    }

    protected function getConfig(): array
    {
        return include('config.php');
    }
}
