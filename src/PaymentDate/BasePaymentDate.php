<?php

namespace Allop\PaymentDate;

Use Allop\TimeValue\Month;
Use Allop\TimeValue\Day;

class BasePaymentDate extends PaymentDate 
{
    private $previousDayOfWeek = Day::Friday;

    public function __construct(Month $month)
    {
        $config = $this->getConfig()['base'];
        $this->previousDayOfWeek = $config['previousDayOfWeek'];

        $defaultDate = $this->getCarbonDate($month)->lastOfMonth();

        $this->date = $defaultDate->isWeekend() ? $defaultDate->previous($this->previousDayOfWeek) : $defaultDate;
    }
}