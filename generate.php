<?php
require_once('vendor/autoload.php');

use Allop\Generator;

$fullYear = getConsoleOption('full-year', false);
$fileFormat = getConsoleOption('format', 'csv');

validateFileFormat($fileFormat);

saveDataToFile(Generator::preparePaymentDates($fullYear), $fileFormat);

echo "Dane zostały pomyślnie zapisane.";


function getConsoleOption(string $name, $defaultValue = null)
{
    $value = getopt(null, [$name.':']);

    return reset($value) ?: $defaultValue;
}

function validateFileFormat($format): void
{
    $supportedTypes = ['csv'];

    if (!in_array($format, $supportedTypes)) {
        die('Unsupported file type. Supported types: ' . implode(', ', $supportedTypes));
    }
}

function saveDataToFile(array $data, string $fileFormat): void
{
    $fileName = 'payment_dates.' . $fileFormat;
    
    $file = fopen($fileName, "w");

    switch($fileFormat) {
        case 'csv':
            foreach ($data as $d) {
                fputcsv($file, $d);
            } 
            break;
    }
    fclose($file);

    header('Content-Encoding: UTF-8');
    header('Content-type: text/' . $fileFormat . '; charset=UTF-8');
    header('Content-Disposition: attachment; filename=' . $fileName);
    readfile($fileName);
}